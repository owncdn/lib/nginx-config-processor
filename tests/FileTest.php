<?php
/**
 * This file is part of the romanpitak/nginx-config-processor package.
 *
 * (c) Roman Piták <roman@pitak.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests;

use wanlxp\Nginx\Config\Scope;
use wanlxp\Nginx\Config\Comment;
use wanlxp\Nginx\Config\Directive;
use wanlxp\Nginx\Config\Exception;
use wanlxp\Nginx\Config\File;
use wanlxp\Nginx\Config\Printable;
use wanlxp\Nginx\Config\Text;
use wanlxp\Nginx\Config\EmptyLine;

class FileTest extends BaseTestCase
{

    /**
     * Fail on non existing file
     *
     * @expectedException \wanlxp\Nginx\Config\Exception
     */
    public function testCannotRead()
    {
        new File('this_file_does_not_exist.txt');
    }

}
