<?php
/**
 * This file is part of the romanpitak/nginx-config-processor package.
 *
 * (c) Roman Piták <roman@pitak.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests;

use wanlxp\Nginx\Config\Scope;
use wanlxp\Nginx\Config\Comment;
use wanlxp\Nginx\Config\Directive;
use wanlxp\Nginx\Config\Exception;
use wanlxp\Nginx\Config\File;
use wanlxp\Nginx\Config\Printable;
use wanlxp\Nginx\Config\Text;
use wanlxp\Nginx\Config\EmptyLine;

class EmptyLineTest  extends BaseTestCase
{

    public function testCanBeConstructed()
    {
        $emptyLine = new EmptyLine();
        $this->assertInstanceOf('\\wanlxp\\Nginx\\Config\\EmptyLine', $emptyLine);
        return $emptyLine;
    }

    /**
     * @depends testCanBeConstructed
     *
     * @param EmptyLine $emptyLine
     */
    public function testPrettyPrint(EmptyLine $emptyLine)
    {
        $this->assertEquals("\n", $emptyLine->prettyPrint(0));
    }

}
