<?php
/**
 * This file is part of the romanpitak/nginx-config-processor package.
 *
 * (c) Roman Piták <roman@pitak.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests;

use wanlxp\Nginx\Config\Scope;
use wanlxp\Nginx\Config\Comment;
use wanlxp\Nginx\Config\Directive;
use wanlxp\Nginx\Config\Exception;
use wanlxp\Nginx\Config\File;
use wanlxp\Nginx\Config\Printable;
use wanlxp\Nginx\Config\Text;
use wanlxp\Nginx\Config\EmptyLine;

class CommentTest extends BaseTestCase
{

    public function testGetText()
    {
        $comment = new Comment('c');
        $this->assertEquals("c", $comment->getText());
    }

    public function testToString()
    {
        $comment = new Comment('c');
        $this->assertEquals("# c\n", (string) $comment);
    }

}
